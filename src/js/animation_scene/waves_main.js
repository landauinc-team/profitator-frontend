import fragment from "./shader/fragment-waves-bg.glsl";
import vertex from "./shader/vertexParticles.glsl";

import { WebGLRenderer } from "three/src/renderers/WebGLRenderer.js";
import { Scene } from "three/src/scenes/Scene.js";
import { PerspectiveCamera } from "three/src/cameras/PerspectiveCamera.js";
import { ShaderMaterial } from "three/src/materials/ShaderMaterial";
import { DoubleSide } from "three/src/constants";
import {
  PlaneBufferGeometry,
  PlaneGeometry
} from "three/src/geometries/PlaneGeometry";
import { Points } from "three/src/objects/Points";
import { Vector2 } from "three/src/math/Vector2";

export default class WavesMain {
  constructor(selector) {
    this.scene = new Scene();

    this.renderer = new WebGLRenderer({ antialias: true, alpha: true });
    this.width = window.innerWidth;
    this.height = window.innerWidth;
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(this.width, this.height);
    this.renderer.setClearColor(0x1b1f22, 0);

    this.container = document.querySelector(selector);
    this.container.appendChild(this.renderer.domElement);

    this.camera = new PerspectiveCamera(
      45,
      window.innerWidth / window.innerHeight,
      0.001,
      1000
    );

    this.camera.position.set(0, -0.02, 0.6);

    this.time = 0;

    this.paused = false;

    this.setupResize();

    this.resize();
    this.addObjects();
    this.render();
    this.settings();
  }

  settings() {
    let that = this;
    this.settings = {
      time: 0,
      size: 0.5,
      amplitude: 0.2,
      curvyness: 3,
      speed: 0.5
    };
  }

  setupResize() {
    window.addEventListener("resize", this.resize.bind(this));
  }

  resize() {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    this.renderer.setSize(this.width, this.height);
    this.camera.aspect = this.width / this.height;
    this.camera.updateProjectionMatrix();
  }

  addObjects() {
    let that = this;
    this.material = new ShaderMaterial({
      extensions: {
        derivatives: "#extension GL_OES_standard_derivatives : enable"
      },
      side: DoubleSide,
      uniforms: {
        time: { type: "f", value: 0 },
        size: { type: "f", value: 1 },
        speed: { type: "f", value: 1 },
        curvyness: { type: "f", value: 2 },
        amplitude: { type: "f", value: 0.4 },
        pixels: {
          type: "v2",
          value: new Vector2(window.innerWidth, window.innerHeight)
        },
        uvRate1: {
          value: new Vector2(1, 1)
        }
      },
      vertexShader: vertex,
      fragmentShader: fragment
    });

    this.geometry = new PlaneBufferGeometry(2, 1, 280, 80);

    this.points = new Points(this.geometry, this.material);
    this.points.rotation.x = Math.PI / 2 + 0.5;
    this.points.rotation.z = 0.3;
    this.scene.add(this.points);
  }

  render() {
    if (this.paused) return;
    this.time -= 0.05;
    this.material.uniforms.time.value = this.time;
    this.material.uniforms.size.value = this.settings.size;
    this.material.uniforms.amplitude.value = this.settings.amplitude;
    this.material.uniforms.curvyness.value = this.settings.curvyness;
    this.material.uniforms.speed.value = this.settings.speed;
    requestAnimationFrame(this.render.bind(this));
    this.renderer.render(this.scene, this.camera);
  }
}
