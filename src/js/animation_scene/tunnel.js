var points = [
  [68.5, 185.5],
  [1, 262.5],
  [270.9, 281.9],
  [345.5, 212.8],
  [178, 155.7],
  [240.3, 72.3],
  [153.4, 0.6],
  [52.6, 53.3],
  [68.5, 185.5]
];

export default class Tunnel {
  constructor() {
    this.scene = new THREE.Scene();
    this.scene.fog = new THREE.Fog(0x1b1f22, 30, 150);

    this.camera = new THREE.PerspectiveCamera(
      45,
      window.innerWidth / window.innerWidth,
      0.1,
      150
    );
    this.camera.position.y = 400;
    this.camera.position.z = 400;
    this.camera.filmOffset = -20;

    this.renderer = new THREE.WebGLRenderer({
      antialias: true,
      alpha: true
    });
    this.width = window.innerWidth;
    this.height = window.innerWidth;
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(this.width, this.height);
    this.renderer.setClearColor(this.scene.fog.color, 0);

    this.container = document.getElementById("container");
    this.container.appendChild(this.renderer.domElement);

    this.setupResize();
    this.resize();
    this.addObjects();
    this.render();
  }

  setupResize() {
    window.addEventListener("resize", this.resize.bind(this));
  }

  resize() {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    this.renderer.setSize(this.width, this.height);
    this.camera.aspect = this.width / this.height;
    this.camera.updateProjectionMatrix();
  }

  addObjects() {
    let pointsArr = [];
    for (var i = 0; i < points.length; i++) {
      var x = points[i][0];
      var y = 100;
      var z = points[i][1];
      pointsArr[i] = new THREE.Vector3(x, y, z);
    }

    this.path = new THREE.CatmullRomCurve3(pointsArr);
    this.path.closed = true;

    var tubeDetail = 1000;
    var circlesDetail = 30;
    var radius = 3;
    var frames = this.path.computeFrenetFrames(tubeDetail, true);
    var geometry = new THREE.Geometry();
    var color = new THREE.Color(0x725bc0);

    for (var i = 0; i < tubeDetail; i++) {
      var normal = frames.normals[i];
      var binormal = frames.binormals[i];
      var index = i / tubeDetail;
      var p = this.path.getPointAt(index);

      for (var j = 0; j < circlesDetail; j++) {
        var position = p.clone();
        var angle = (j / circlesDetail) * Math.PI * 2 + index * Math.PI * 5;
        var sin = Math.sin(angle);
        var cos = -Math.cos(angle);

        var normalPoint = new THREE.Vector3(0, 0, 0);
        normalPoint.x = cos * normal.x + sin * binormal.x;
        normalPoint.y = cos * normal.y + sin * binormal.y;
        normalPoint.z = cos * normal.z + sin * binormal.z;
        normalPoint.multiplyScalar(radius);

        position.add(normalPoint);
        geometry.colors.push(color);
        geometry.vertices.push(position);
      }
    }

    var material = new THREE.PointsMaterial({
      size: 0.25,
      sizeAttenuation: true,
      transparent: true,
      depthWrite: false,
      map: createCanvasMaterial("#beabff", 256),
      vertexColors: THREE.VertexColors
    });

    var tube = new THREE.Points(geometry, material);
    this.scene.add(tube);
    this.percentage = 0;
  }

  render() {
    if (this.paused) return;
    this.percentage += 0.00005;
    var p1 = this.path.getPointAt(this.percentage % 1);
    var p2 = this.path.getPointAt((this.percentage + 0.01) % 1);
    this.camera.position.set(p1.x, p1.y, p1.z);
    this.camera.lookAt(p2);
    requestAnimationFrame(this.render.bind(this));
    this.renderer.render(this.scene, this.camera);
  }
}

function createCanvasMaterial(color, size) {
  var matCanvas = document.createElement("canvas");
  matCanvas.width = matCanvas.height = size;
  var matContext = matCanvas.getContext("2d");
  var texture = new THREE.Texture(matCanvas);
  var center = size / 2;
  matContext.beginPath();
  matContext.arc(center, center, size / 2, 0, 2 * Math.PI, false);
  matContext.closePath();
  matContext.fillStyle = color;
  matContext.fill();
  texture.needsUpdate = true;
  return texture;
}

export { createCanvasMaterial };
