uniform float time;
uniform float progress;
uniform sampler2D texture1;
uniform sampler2D texture2;
uniform vec2 pixels;
uniform vec2 uvRate1;
uniform vec2 accel;

varying vec2 vUv;
varying vec2 vUv1;
varying vec4 vPosition;
varying vec3 vNormal;

void main(){
	
	vec3 color1=vec3(.4470,.3568,.7529);
    vec3 color2=vec3(.3294,.2549,.5725);
	
	vec3 light=normalize(vec3(1.,1.,1.));
	float l=dot(vNormal,light);
	l=max(0.,l);
	
	vec3 finalColor=mix(color1,color2,l);
	
	// vec4 color = texture2D(texture1,gl_PointCoord);
	// if(color.r<0.01) discard;
	
	if(length(gl_PointCoord-vec2(.5,.5))>.475)discard;
	// gl_FragColor=vec4(1.,0.,0.,1.);
	// gl_FragColor=vec4(.737,.656,1.,1.);
	// gl_FragColor=vec4(l,l,l,1.);
	gl_FragColor=vec4(finalColor,1.);
	// gl_FragColor = vec4( vNormal, 1.0 );
	
	// gl_FragColor = vec4(1.0,0.0,0.0,1.);
}