var clock = new THREE.Clock();

export default class Particles {
  constructor(selector) {
    this.scene = new THREE.Scene();

    this.camera = new THREE.PerspectiveCamera(
      75,
      window.innerWidth / window.innerHeight,
      0.01,
      100
    );
    this.camera.position.set(0, 1, 15);

    this.renderer = new THREE.WebGLRenderer({
      antialias: true,
      alpha: true
    });
    this.width = window.innerWidth;
    this.height = window.innerWidth;
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(this.width, this.height);
    this.renderer.setClearColor(0x1b1f22, 0);

    this.container = document.getElementById("container");
    this.container.appendChild(this.renderer.domElement);

    this.setupResize();
    this.resize();
    this.addObjects();
    this.render();
  }

  setupResize() {
    window.addEventListener("resize", this.resize.bind(this));
  }

  resize() {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    this.renderer.setSize(this.width, this.height);
    this.camera.aspect = this.width / this.height;
    this.camera.updateProjectionMatrix();
  }

  addObjects() {
    var flakeCount = 2000;
    var flakeGeometry = new THREE.SphereBufferGeometry(0.02, 10, 10);
    var flakeMaterial = new THREE.MeshPhongMaterial({ color: 0x725bc0 });
    this.snow = new THREE.Group();

    for (let i = 0; i < flakeCount; i++) {
      var flakeMesh = new THREE.Mesh(flakeGeometry, flakeMaterial);
      flakeMesh.position.set(
        (Math.random() - 0.5) * 40,
        (Math.random() - 0.5) * 20,
        (Math.random() - 0.5) * 40
      );
      this.snow.add(flakeMesh);
    }
    this.scene.add(this.snow);

    this.flakeArray = this.snow.children;

    var ambientLight = new THREE.AmbientLight(0xffffff, 0.8);
    this.scene.add(ambientLight);
  }

  render() {
    if (this.paused) return;
    for (let i = 0; i < this.flakeArray.length / 2; i++) {
      this.flakeArray[i].rotation.y += 0.01;
      this.flakeArray[i].rotation.x += 0.02;
      this.flakeArray[i].rotation.z += 0.03;
      this.flakeArray[i].position.y -= 0.018;
      if (this.flakeArray[i].position.y < -4) {
        this.flakeArray[i].position.y += 10;
      }
    }
    for (let i = this.flakeArray.length / 2; i < this.flakeArray.length; i++) {
      this.flakeArray[i].rotation.y -= 0.03;
      this.flakeArray[i].rotation.x -= 0.03;
      this.flakeArray[i].rotation.z -= 0.02;
      this.flakeArray[i].position.y -= 0.016;
      if (this.flakeArray[i].position.y < -4) {
        this.flakeArray[i].position.y += 9.5;
      }
      this.snow.rotation.y -= 0.0000002;
    }
    requestAnimationFrame(this.render.bind(this));
    this.renderer.render(this.scene, this.camera);
  }
}
