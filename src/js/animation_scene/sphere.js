var clock = new THREE.Clock();

export default class Sphere {
  constructor(radius) {
    this.scene = new THREE.Scene();

    this.camera = new THREE.PerspectiveCamera(
      75,
      window.innerWidth / window.innerHeight,
      0.1,
      1000
    );
    this.camera.filmOffset = -25;
    this.camera.position.set(0, 0, 150);

    this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    this.width = window.innerWidth;
    this.height = window.innerWidth;
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(this.width, this.height);
    this.renderer.setClearColor(0x1b1f22, 0);

    this.container = document.getElementById("container");
    this.container.appendChild(this.renderer.domElement);

    this.setupResize();
    this.resize();
    this.addObjects(radius);
    this.render();
  }

  setupResize() {
    window.addEventListener("resize", this.resize.bind(this));
  }

  resize() {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    this.renderer.setSize(this.width, this.height);
    this.camera.aspect = this.width / this.height;
    this.camera.updateProjectionMatrix();
  }

  addObjects(radius) {
    this.group = new THREE.Object3D();

    var separation = 10;

    for (var s = 0; s <= 180; s += separation) {
      var radianS = (s * Math.PI) / 180;
      var pZ = radius * Math.cos(radianS);

      for (var t = 0; t < 360; t += separation) {
        var radianT = (t * Math.PI) / 180;
        var pX = radius * Math.sin(radianS) * Math.cos(radianT);
        var pY = radius * Math.sin(radianS) * Math.sin(radianT);

        var geometory = new THREE.SphereGeometry(1, 6, 6);
        var material = new THREE.MeshBasicMaterial({
          color: 0x725bc0
        });
        var mesh = new THREE.Mesh(geometory, material);
        mesh.position.x = pX;
        mesh.position.y = pY;
        mesh.position.z = pZ;
        this.group.add(mesh);
      }
    }

    this.scene.add(this.group);
  }

  render() {
    if (this.paused) return;
    var rotateX = this.group.rotation.x + 0.0001;
    var rotateY = this.group.rotation.y + 0.001;
    var rotateZ = this.group.rotation.z + 0.005;
    this.group.rotation.set(rotateX, rotateY, rotateZ);

    var t = clock.getElapsedTime();

    if (t <= 1) {
      this.group.scale.x -= 0.001;
      this.group.scale.y -= 0.001;
      this.group.scale.z -= 0.001;
    } else if (t >= 2) {
      clock = new THREE.Clock();
    } else {
      this.group.scale.x += 0.001;
      this.group.scale.y += 0.001;
      this.group.scale.z += 0.001;
    }

    this.camera.lookAt(this.scene.position);
    requestAnimationFrame(this.render.bind(this));
    this.renderer.render(this.scene, this.camera);
  }
}
