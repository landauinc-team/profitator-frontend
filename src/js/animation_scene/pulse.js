import { createCanvasMaterial } from "./tunnel";

export default class Pulse {
  constructor() {
    this.scene = new THREE.Scene();
    this.scene.fog = new THREE.FogExp2(0x1b1f22, 0.006);

    this.camera = new THREE.PerspectiveCamera(
      75,
      window.innerWidth / window.innerHeight,
      0.0001,
      100000
    );
    this.camera.position.set(0, 45, 100);
    this.camera.filmOffset = -25;

    this.renderer = new THREE.WebGLRenderer({
      antialias: true,
      alpha: true
    });
    this.width = window.innerWidth;
    this.height = window.innerWidth;

    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(this.width, this.height);
    this.renderer.setClearColor(this.scene.fog.color, 0);

    this.container = document.getElementById("container");
    this.container.appendChild(this.renderer.domElement);

    this.setupResize();
    this.resize();
    this.addObjects();
    this.render();
  }

  setupResize() {
    window.addEventListener("resize", this.resize.bind(this));
  }

  resize() {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    this.renderer.setSize(this.width, this.height);
    this.camera.aspect = this.width / this.height;
    this.camera.updateProjectionMatrix();
  }

  addObjects() {
    var planeGeometry = new THREE.PlaneGeometry(1000, 1000, 200, 200);

    var planeMaterial = new THREE.MeshBasicMaterial({
      color: 0x725bc0,
      wireframe: true
    });

    this.plane = new THREE.Mesh(planeGeometry, planeMaterial);
    this.scene.add(this.plane);

    var particlesGeometry = new THREE.Geometry();
    particlesGeometry.vertices = planeGeometry.vertices;

    var particlesMaterial = new THREE.PointsMaterial({
      color: 0xbeabff,
      size: 1.5,
      transparent: true,
      depthWrite: false,
      map: createCanvasMaterial("#beabff", 256)
    });

    this.particles = new THREE.Points(particlesGeometry, particlesMaterial);
    this.particles.rotation.x = -0.5 * Math.PI;
    this.particles.position.set(0, 0, 0);
    this.scene.add(this.particles);

    var ambientLight = new THREE.AmbientLight(0xffffff);
    this.scene.add(ambientLight);
  }

  render(ts) {
    if (this.paused) return;

    var center = new THREE.Vector2(0, 0);
    var vLength = this.particles.geometry.vertices.length;
    for (var i = 0; i < vLength; i++) {
      var v = this.particles.geometry.vertices[i];
      var dist = new THREE.Vector2(v.x, v.y).sub(center);
      var size = 5.0;
      var magnitude = 4;
      v.z = Math.sin(dist.length() / -size + ts / 500) * magnitude;
    }
    this.particles.geometry.verticesNeedUpdate = true;
    this.camera.lookAt(this.scene.position);
    requestAnimationFrame(this.render.bind(this));
    this.renderer.render(this.scene, this.camera);
  }
}
