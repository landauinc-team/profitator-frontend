import './common/header.js';
import './pages/main_page.js';
import './pages/service.js';
import './common/form.js';
import './common/animation.js';
import './common/cookie.js';
import './common/modal.js';