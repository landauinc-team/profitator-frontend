import Swiper from 'swiper/dist/js/swiper.js';
// import lottie from "lottie-web";

var serviceSlider = new Swiper('.swiper-service', {
    autoplay: {
        delay: 5000,
    },
    speed: 500,
    spaceBetween: 100,
    pagination: {
        el: '.service-pagination',
        type: 'bullets',
        clickable: true,
        renderBullet: function(index, className) {
            return `<span class="${className}">0${index + 1}</span>`;
        }
    },
});

// lottie.loadAnimation({
//   container: document.body, // the dom element that will contain the animation
//   renderer: 'svg',
//   loop: true,
//   autoplay: true,
//   path: '/assets/js/animation1.json' // the path to the animation json
// });