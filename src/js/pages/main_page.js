import Swiper from 'swiper/dist/js/swiper.js';
import Waves from "../animation_scene/waves";
import Sphere from "../animation_scene/sphere";
import Particles from "../animation_scene/particles";
import Pulse from "../animation_scene/pulse";
import Tunnel from "../animation_scene/tunnel";
import $ from "jquery";

let mainSlider = undefined;

if (document.querySelector('.main_page')) {
  initMainSlider();
  switchScene(0);
  animateSlideContentMobile();
  window.addEventListener('resize', initMainSlider);
  window.addEventListener('resize', animateSlideContentMobile);
  $(window).scroll(animateSlideContentMobile);
}

let requestAnimationFrame =
    window.requestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.msRequestAnimationFrame;

let cancelAnimationFrame =
    window.cancelAnimationFrame || window.mozCancelAnimationFrame;

let raf;
let scene;
let opacity = 1;
let num;

function switchScene(numScene) {
  switch (numScene) {
    case 0:
      scene = new Waves(0.7,0.4,2, 0.1, 0.3);
      break;
  
    case 5:
      scene = new Waves(0.45,0.05,5.45, 0.1, 1.5);
      break;
  
    case 1:
      scene = new Sphere(120);
      break;
  
    case 6:
      scene = new Sphere(200);
      break;
      
    case 2:
    case 7:
      scene = new Particles();
      break;
    
    case 3:
    case 8:
      scene = new Pulse();
      break;
    
    case 4:
    case 9:
      scene = new Tunnel();
      break;
  }
}

function animate(numScene) {
  scene.container.children[0].style.opacity = opacity;
  num = numScene;
  animateSceneOut();
}

function animateSceneOut() {
  scene.scene.scale.x += 0.4;
  scene.scene.scale.y += 0.4;
  scene.scene.scale.z += 0.4;
  opacity -= 0.04;
  scene.container.children[0].style.opacity = opacity;
  raf = requestAnimationFrame(animateSceneOut);
  if (scene.scene.scale.x > 10) {
    cancelAnimationFrame(raf);
    scene.container.children[0].parentNode.removeChild(scene.container.children[0]);
    scene.paused = true;
    switchScene(num);
    if (num !== 4) {
      scene.scene.scale.x = 10;
      scene.scene.scale.y = 10;
      scene.scene.scale.z = 10;
    }
    opacity = 0;
    scene.container.children[0].style.opacity = opacity;
    animateSceneIn();
  }
}

function animateSceneIn() {
  if (num !== 4) {
    scene.scene.scale.x -= 0.4;
    scene.scene.scale.y -= 0.4;
    scene.scene.scale.z -= 0.4;
  }
  opacity += 0.04;
  scene.container.children[0].style.opacity = opacity;
  
  raf = requestAnimationFrame(animateSceneIn);
  
  if (opacity > 0.9) {
    cancelAnimationFrame(raf);
  }
}

function initMainSlider() {
  var screenWidth = window.innerWidth;
  
  if (screenWidth > 560 && mainSlider === undefined) {
    mainSlider = new Swiper(".main_page .swiper-container", {
      speed: 700,
      direction: 'vertical',
      simulateTouch: false,
      mousewheel: {
        enabled: true,
      },
      keyboard: {
        enabled: true,
      },
      pagination: {
        el: ".main_page .swiper-pagination",
        clickable: true,
        renderBullet: function (index, className) {
          return '<span class="' + className + '"> 0' + index + "</span>";
        }
      }
    });
    
    mainSlider.on("slideChange", function () {
      animate(mainSlider.activeIndex);
      document.querySelector('.index_slide').innerText = '0' + mainSlider.activeIndex;
      animateSlideContent(mainSlider.activeIndex);
      
      if (mainSlider.activeIndex === 0) {
        document.querySelector('.index_slide').innerText = '';
      }
  
      if (mainSlider.isEnd) {
        document.querySelector('.scroll-icon').style = 'opacity: 0';
      } else {
        document.querySelector('.scroll-icon').style = 'opacity: 1';
      }
    });
  } else if (screenWidth <= 560 && mainSlider !== undefined) {
    mainSlider.destroy(false, true);
    mainSlider = undefined;
  }
}

function animateSlideContent(i) {
  $('.main-section .swiper-slide').each(function (index, element) {
    if (i !== index) return;
    
    TweenMax.to($(element).find('h2'), 1, {delay: 0.5, opacity: 1, left: 0, ease: Sine.easeInOut});
    TweenMax.to($(element).find('.slide_col__top'), 0.8, {delay: 1, opacity: 1, right: 0, ease: Sine.easeInOut});
    TweenMax.to($(element).find('.slide_col__bottom'), 0.8, {delay: 1.5, opacity: 1, bottom: 0, ease: Sine.easeInOut});
  });
}

function animateSlideContentMobile() {
  if ($(window).width() > 560) return;
  
  $.fn.isInViewport = function () {
    let elementTop = $(this).offset().top + 100,
        elementBottom = elementTop + $(this).outerHeight(),
        viewportTop = $(window).scrollTop(),
        viewportBottom = viewportTop + $(window).height();
    
    return elementBottom > viewportTop && elementTop < viewportBottom;
  };
  
  let scrollTop = $(window).scrollTop();
  
  $('.main-section .swiper-slide').each(function (index, element) {
    if ($(element).isInViewport() || scrollTop >= $(element).offset().top) {
      TweenMax.to($(element).find('h2'), 0.3, { opacity: 1, y: 0, ease: Sine.easeInOut});
      TweenMax.to($(element).find('.slide_col__top'), 0.3, {delay: 0.1, opacity: 1, y: 0, ease: Sine.easeInOut});
      TweenMax.to($(element).find('.slide_col__bottom'), 0.3, {delay: 0.2, opacity: 1, y: 0, ease: Sine.easeInOut});
    }
  });
}