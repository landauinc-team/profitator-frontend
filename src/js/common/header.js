import $ from "jquery";

const menuBtn = document.querySelector('.js-hamburger-menu');
const header = document.querySelector('.header');

menuBtn.addEventListener('click', handelMenu);
document.body.addEventListener('click', closeMobileMenu);

$(window).scroll(stickyHeader);
stickyHeader();

$(window).resize(function () {
  if ($(window).width() > 768) {
    closeMobileMenu(this);
  }
});

function handelMenu(e) {
  if (!$(this).hasClass('open')) {
    disabledScroll();
    $('.js-mobile-menu').fadeIn(100);
    e.target.closest('.js-hamburger-menu').classList.add('open');
    e.target.closest('.header').classList.add('open');
    $('main, footer').addClass('blur');
  } else {
    e.target.closest('.js-hamburger-menu').classList.remove('open');
    e.target.closest('.header').classList.remove('open');
    $('main, footer').removeClass('blur');
    $('.js-mobile-menu').fadeOut(200);
    $('body').removeClass('scrollDisabled').css('margin-top', 0);
  }
}

function closeMobileMenu(e) {
  if ($(e.target).closest('.header').length > 0 ||
      $(e.target).closest('.js-open-modal').length > 0 ||
      $(e.target).closest('.js-open-modal-resume').length > 0 ||
      $(window).width() > 560
  ) return;
  
  $('.js-hamburger-menu').removeClass('open');
  $('.header').removeClass('open');
  $('main, footer').removeClass('blur');
  
  $('body').removeClass('scrollDisabled').css('margin-top', 0);
  $('.js-mobile-menu').fadeOut(200);
}

function stickyHeader() {
  if ($(window).scrollTop() > 10) {
    header.classList.add('sticky')
  } else {
    header.classList.remove('sticky')
  }
}

function disabledScroll() {
  let y_offsetWhenScrollDisabled = 0;
  y_offsetWhenScrollDisabled= $(window).scrollTop();
  $('body').addClass('scrollDisabled').css('margin-top', -y_offsetWhenScrollDisabled);
}

export { disabledScroll };
