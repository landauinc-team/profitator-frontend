import WavesMain from "../animation_scene/waves_main";
import $ from "jquery";

$(window).scroll(showSections);
showSections();

$('.droptitle').hover(animateDropdownIn, animateDropdownOut);

if ($('#main_bg').length > 0) {
  new WavesMain('#main_bg');
}

if ($('#bg_wave').length > 0) {
  new WavesMain('#bg_wave');
}

function showSections() {
  $.fn.isInViewport = function () {
    let elementTop = $(this).offset().top + 100,
        elementBottom = elementTop + $(this).outerHeight(),
        viewportTop = $(window).scrollTop(),
        viewportBottom = viewportTop + $(window).height();
    
    if ($(window).width() <= 580) elementTop = $(this).offset().top + 30;
    
    return elementBottom > viewportTop && elementTop < viewportBottom;
  };
  
  var scrollTop = $(window).scrollTop();
  
  $('section:not(.main) > *').each(function (index, element) {
    if ($(element).isInViewport() || scrollTop >= $(element).offset().top) {
      TweenMax.to($(element), 0.5, {opacity: 1, y: 0, ease: Sine.easeInOut});
    }
  });
  
  if ($('.service-slider').length) {
    if ($('.service-slider').isInViewport() || scrollTop >= $('.service-slider').offset().top) {
      TweenMax.to($('.service-slider .service-pagination'), 0.8, {
        delay: 0.5,
        autoAlpha: 1,
        left: '0px',
        ease: Power1.easeOut
      });
      TweenMax.to($('.service-slider .swiper-slide-text'), 0.8, {
        delay: 0.5,
        autoAlpha: 1,
        left: '0px',
        ease: Power1.easeOut
      });
      TweenMax.to($('.service-slider .swiper-slide-card'), 0.8, {
        delay: 1,
        autoAlpha: 1,
        right: '0px',
        ease: Power1.easeOut
      });
    }
  }
  
  if ($('.result-wrapper').length) {
    $('.result-wrapper > div').each(function (index, element) {
      if ($(element).isInViewport() || scrollTop >= $(element).offset().top) {
        TweenMax.to($(element), 0.5, {delay: 0.4 * index, opacity: 1, ease: Sine.easeInOut});
      }
    });
  }
  
  if ($('.service-wrapper').length) {
    $('.service-wrapper-item').each(function (index, element) {
      if ($(element).isInViewport() || scrollTop >= $(element).offset().top) {
        TweenMax.to($(element).children('div'), 0.5, {delay: 0.2, opacity: 1, left: 0, ease: Sine.easeInOut});
        TweenMax.to($(element).children('img'), 0.5, {delay: 0.4, opacity: 1, right: 0, ease: Sine.easeInOut});
      }
    });
  }
  
  if ($('.jobs-wrapper').length) {
    $('.jobs-wrapper-item').each(function (index, element) {
      if ($(element).isInViewport() || scrollTop >= $(element).offset().top) {
        TweenMax.to($(element), 0.5, {delay: 0.2 * index, opacity: 1, ease: Sine.easeInOut});
      }
    });
  }
}

function animateDropdownIn() {
  TweenMax.to($('.droptitle__menu'), 0.3, {visibility: 'inherit', opacity: 1, top: '100%', ease: Sine.easeInOut});
  
  $('.droptitle__menu a').each(function (index, element) {
    TweenMax.to($(element), 0.3, {delay: 0.2 * index, opacity: 1, y: 0, ease: Sine.easeInOut});
  });
}

function animateDropdownOut() {
  TweenMax.to($('.droptitle__menu'), 0.5, {visibility: 'hidden', opacity: 0, top: 0, ease: Sine.easeInOut});
  
  $('.droptitle__menu a').each(function (index, element) {
    TweenMax.to($(element), 0.5, {opacity: 0, y: '-10px', ease: Sine.easeInOut});
  });
}
