import $ from "jquery";
import Inputmask from "inputmask";
import "babel-polyfill";

const inputs = document.querySelectorAll('.mpf_input');
const fileInput = document.querySelector('input[type="file"]');
const forms = document.querySelectorAll('form');
const phones = document.querySelectorAll("input[name='phone']");
let error = false;
let errorPhone = false;
const emailMask = /[0-9a-z_]+@[0-9a-z_^.]+\.[a-z]{2,3}/i;

for (let i = 0; i < phones.length; i++) {
  let phone = phones[i];
  
  Inputmask({
    "mask": "+7 (999) 999-9999",
    showMaskOnHover: false,
    onincomplete: function () {
      phone.classList.add('error');
      errorPhone = true;
    },
    oncomplete: function () {
      phone.classList.add('success');
      errorPhone = false;
    }
  }).mask(phone);
}

for (let i = 0; i < inputs.length; i++) {
  let input = inputs[i];
  
  input.addEventListener('focus', moveLabel);
  input.addEventListener('click', moveLabel);
  input.addEventListener('input', removeValidateClass);
}

if (fileInput) {
  fileInput.addEventListener('change', loadFiles);
}

for (let i = 0; i < forms.length; i++) {
  let form = forms[i];
  
  form.addEventListener('submit', handleForm);
}

function moveLabel(e) {
  for (let i = 0; i < inputs.length; i++) {
    if (inputs[i].value === '') {
      inputs[i].previousElementSibling.classList.remove('focus');
    }
  }
  
  e.target.previousElementSibling.classList.add('focus');
}

function loadFiles() {
  var files = Array.from(this.files);
  var $attachedFilesList = $(this).closest('.form_block').find('.files__btn');
  
  $attachedFilesList.html('Перетащите файл или <span>загрузите</span>');
  
  if (files.length > 0) {
    $attachedFilesList.html('');
    for (var i = 0; i < files.length; i++) {
      $attachedFilesList.append('<div class="file__name">' + files[i].name + ' <span class="del js-delete"></span></div>');
    }
  }
  
  $('.js-delete').on('click', function (e) {
    e.preventDefault();
    let file = $(this).closest('.file__name');
    files.splice(file.index(), 1);
    file.remove();
  });
}

function handleForm(e) {
  e.preventDefault();
  
  validate(this);
  
  if (error || errorPhone) return;
  
  if ($(this).hasClass('form-submitted')) {
    return false;
  }
  
  this.classList.add('form-submitted');
  this.querySelector('button[type="submit"]').setAttribute("disabled", "true");
  
  sendForm(this)
}

function sendForm(form) {
  $.ajax({
    url: '/send.php',
    method: 'POST',
    data: new FormData(form),
    processData: false,
    contentType: false,
    dataType: "JSON",
    success: function (data) {
      $(form).trigger('reset');
      $('.mpf_input').removeClass('success');
      $('.form_block label').removeClass('focus');
      $(form).fadeOut(300);
      
      setTimeout(function () {
        $(form).closest('.modal-form-wrap').find('.thank-block').fadeIn(300);
        $(form).next('.main_page_thank').fadeIn(300);
      }, 300);
    },
    error: function (data) {
      console.log(data);
    },
    complete: function () {
      $(form).removeClass('form-submitted');
      $(form).find('.button-big').removeAttr('disabled');
    }
  })
}

function validate(form) {
  error = false;
  
  for (let i = 0; i < form.querySelectorAll('.mpf_input').length; i++) {
    let input = form.querySelectorAll('.mpf_input')[i];
  
    if (input.value === '') {
      input.classList.add('error');
      error = true;
    } else {
      input.classList.add('success');
    }
  }
  
  if (errorPhone) {
    form.querySelector("input[name='phone']").classList.add('error');
    form.querySelector("input[name='phone']").classList.remove('success');
  }
  
  if (form.querySelector("input[name='email']")) {
    if (!emailMask.test(form.querySelector("input[name='email']").value)) {
      form.querySelector("input[name='email']").classList.add('error');
      form.querySelector("input[name='email']").classList.remove('success');
      error = true;
    }
  }
}

function removeValidateClass(e) {
  if (e.target.classList.contains('error')) e.target.classList.remove('error');
  if (e.target.classList.contains('success')) e.target.classList.remove('success');
}
