import $ from "jquery";
import { disabledScroll } from './header';

document.body.addEventListener('click', openModal);
document.body.addEventListener('click', openModalResume);
document.body.addEventListener('click', closeModal);
document.addEventListener('keydown', closeModal);

function closeModal(e) {
  if ($(e.target).closest('.modal-form-wrap').length > 0 &&
      $(e.target).closest('.js-close-modal').length === 0 ||
      $(e.target).closest('.js-delete').length > 0 ||
      $(e.target).closest('.header').length > 0 ||
      $(e.target).closest('.js-open-modal').length > 0 ||
      $(e.target).closest('.js-open-modal-resume').length > 0) return;
  
  $('header, main, footer').removeClass('blur');
  $('.js-modal, .js-modal-resume').fadeOut(300);
  $('.modal-form-wrap').removeClass('open');
  $('body').removeClass('scrollDisabled').css('margin-top', 0);
  
  $('.modal .thank-block').hide();
  $('.modal-form').show();
  
  if (e.which === 27) closeModal();
}

function openModal(e) {
  if ($(e.target).closest('.js-open-modal').length === 0) return;
  
  e.preventDefault();
  disabledScroll();
  
  $('header, main, footer').addClass('blur');
  $('.js-modal').fadeIn(300);
  $('.modal-form-wrap').addClass('open');
}

function openModalResume(e) {
  if ($(e.target).closest('.js-open-modal-resume').length === 0) return;
  
  e.preventDefault();
  disabledScroll();
  
  $('header, main, footer').addClass('blur');
  $('.js-modal-resume').fadeIn(300);
  $('.modal-form-wrap').addClass('open');
}