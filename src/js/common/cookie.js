import $ from "jquery";

function getCookie(name) {
  let matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options = {}) {
  
  options = {
    path: '/',
    // при необходимости добавьте другие значения по умолчанию
    ...options
  };
  
  if (options.expires.toUTCString) {
    options.expires = options.expires.toUTCString();
  }
  
  let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);
  
  for (let optionKey in options) {
    updatedCookie += "; " + optionKey;
    let optionValue = options[optionKey];
    if (optionValue !== true) {
      updatedCookie += "=" + optionValue;
    }
  }
  
  document.cookie = updatedCookie;
}

function deleteCookie(name) {
  setCookie(name, "", {
    'max-age': -1
  })
}

export { getCookie, setCookie, deleteCookie };

const closeCookie = document.querySelector('.js-close-cookies');

closeCookie.addEventListener('click', closeCookies);

if (!getCookie('was')) {
  $('.cookies').css('display', 'flex');
}

function closeCookies() {
  $('.cookies').fadeOut(300);
  
  let d = new Date();
  d.setTime(d.getTime() + (3600 * 1000 * 24 * 365));
  let expiresDate = d.toUTCString();
  
  setCookie('was', '1', {path: '/', expires: expiresDate});
}