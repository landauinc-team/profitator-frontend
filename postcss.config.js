module.exports = {
  plugins: [
    require('autoprefixer')({
      grid: true,
      browsers: 'last 4 versions'
    }),
    require('css-mqpacker'),
    require('cssnano')({
      preset: [
        'default', {
          discardComments: {
            removeAll: true
          }
        }
      ]
    })
  ]
};